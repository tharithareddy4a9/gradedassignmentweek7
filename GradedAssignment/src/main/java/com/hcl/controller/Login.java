package com.hcl.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hcl.entity.Books;
import com.hcl.service.BooksService;
import com.hcl.service.LoginService;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		Books book = new Books();

		BooksService service = new BooksService();
		
		List<Books> result = service.getAllBooks();
		
		HttpSession session = request.getSession();
		session.setAttribute("obj", result);
		
		RequestDispatcher reqdis = request.getRequestDispatcher("displayBooks.jsp");
		reqdis.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");

		LoginService service = new LoginService();
		
		HttpSession session = request.getSession();
		
		RequestDispatcher reqdis = request.getRequestDispatcher("login.jsp");
		if (request.getParameter("phoneNumber") != null) {
			long mobileNo = Long.parseLong(request.getParameter("phoneNumber"));
			String passcode = request.getParameter("password");
			String result = service.verifyPassword(mobileNo, passcode);
			reqdis.include(request, response);
		}
		doGet(request, response);
	}

}
