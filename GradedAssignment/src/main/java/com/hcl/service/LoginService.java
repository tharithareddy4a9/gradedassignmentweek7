package com.hcl.service;

import com.hcl.dao.LoginDao;
import com.hcl.entity.Login;

public class LoginService {
	LoginDao user = new LoginDao();

	public String storeNewAccount(Login details) {
		if (user.storeNewAccount(details) > 0) {
			return "Sign in Success";
		} else {
			return "unable to create";
		}
	}

	public String verifyPassword(long phoneNumber, String password) {
		if (user.verifyPassword(phoneNumber, password) == true) {
			return "Successfully logged in";
		} else {
			return "Login failed....Try again";
		}
	}

}
