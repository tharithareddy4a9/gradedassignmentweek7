package com.hcl.entity;

public class Login {
	private long phoneNumber;
	private String mailID;
	private String userName;
	private String password;

	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Login(long phoneNumber, String mailID, String userName, String password) {
		super();
		this.phoneNumber = phoneNumber;
		this.mailID = mailID;
		this.userName = userName;
		this.password = password;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMailId() {
		return mailID;
	}

	public void setMailId(String mailID) {
		this.mailID = mailID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
